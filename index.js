const express = require("express");


const mongoose = require("mongoose");

const app = express();
const port = 3001;

mongoose.connect("mongodb+srv://admin:admin@batch230.emvjcz2.mongodb.net/s35?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true 
	}
)

let db = mongoose.connection

db.on("error", console.error.bind(console, "connection error"));
db.once("open", ()=> concole.log("We're connected to the cloud database"));

app.use(express.json());

const taskSchema = new mongoose.Schema({

	name: String,
	status: {
		type: String,
		default: "pending"
	}
}) 

const Task = mongoose.model('Task', taskSchema);

app.post("/task", (request, response) => {

	Task.findOne({name: request.body.name}, (err, result) => {

		if(result!=null && result.name == request.body.name){
		return response.send("Duplicate task found");
		}
		else{

			let newTask = new Task ({
				name: request.body.name
			})

			newTask.save((saveErr, savedTask)=>{
				if(saveErr){
					return console.error(saveErr);
				}
				else{
					return response.status(201).send("New task created");
				}

			})

		}

	})
})

app.get("/task", (request,response)=>{
	Task.find({}, (err, result)=> {
		if(err){
			return console.log(err);
		}
		else{
			return response.status(200).json({
				data : result
			})
		}
	})
})


const userSchema = new mongoose.Schema({

	username: String,
	password: {
		type: String,

	}
}) 

const User = mongoose.model('User', userSchema);

app.post("/signup", (request, response) => {

	User.findOne({username: request.body.username}, {password: request.body.password}, (err, result) => {

		if(result!=null && result.username == request.body.username){
		return response.send("New user registered");
		}
		else{
			let newUser = new User ({
				username: request.body.username,
				password: request.body.password
			})

			newUser.save((saveErr, savedTask)=>{
				if(saveErr){
					return console.error(saveErr);
				}
				else{
					return response.status(201).send("New user registered");
				}


			})

		}

	})


})

app.get("/signup", (request,response)=>{
	Task.find({}, (err, result)=> {
		if(err){
			return console.log(err);
		}
		else{
			return response.status(200).json({
				data : result
			})
		}
	})
})


app.listen(port, () =>console.log(`Server running at port ${port}`));



